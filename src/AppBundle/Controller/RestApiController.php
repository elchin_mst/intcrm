<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class RestApiController extends Controller
{
    /**
     * @Route("/rest")
     */
    public function getAction(Request $request)
    {
        //ссылка на файл и идентификатор звонка
        $recValue = $request->query->get('file_link');
        $recValue = $this->normalString($recValue);


        //file_put_contents('/home/el/log/log', $recValue);

        $callSession = $request->query->get('call_session_id');
        $fileName = '/home/el/log/' . $callSession . '.mp3';
        $fileSize = $request->query->get('file_size');

        //Открыть сессию получить токен(key)

        $jsonKey = $this->login();
        $key = json_decode($jsonKey, true);

        //Скачать файл

        $file = file_get_contents($recValue);
        file_put_contents($fileName, $file);

        //Проверка целостности файла.

        $this->sizeFile($request, $fileName, $recValue);

        //Закрыть сессию

        $closeKey = $key['data']['session_key'];
        $this->logout($closeKey);

        //Закинуть на FTP

        $urlFtp = $this->ftpUpload($fileName, $callSession, $fileSize);


        return new Response($key['data']['session_key']);
    }

    public function login()
    {
        $url = 'http://api.comagic.ru/api/login/?login=marketing@solarfields.ru&password=x5tsIE7y';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    public function logout($sessionKey)
    {
        $url = 'http://api.comagic.ru/api/logout/?session_key=' . $sessionKey;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_close($ch);
    }

    public function sizeFile($recFile, $fileName, $recValue)
    {
        $fileSize = $recFile->query->get('file_size');
        $trueFileSize = filesize($fileName);
        if ($fileSize != $trueFileSize) {
            $file = file_get_contents($recValue);
            file_put_contents($fileName, $file);
        }
    }

    public function ftpUpload($fileName, $callSessionId, $fileSize)
    {
        $newFile = $callSessionId . 'mp3';
        $ftpRes = ftp_connect('ftp.selcdn.ru');
        ftp_login($ftpRes, '4254_solarfieldscalls', 'YGz5fADMys');
        ftp_chdir($ftpRes, '/solarfields-calls/');
        ftp_put(
            $ftpRes, $newFile, $fileName, FTP_BINARY
        );
        if (ftp_size($ftpRes, $newFile) != $fileSize) {
            $this->ftpUpload($fileName, $callSessionId, $fileSize);
        }
        $urlFtp = 'ftp://4254_solarfieldscalls:YGz5fADMys@ftp.selcdn.ru/solarfields-calls/' . $newFile;
        ftp_close($ftpRes);

        return $urlFtp;
    }

    public function normalString($recValue)
    {
        $recValue = trim($recValue, ']');
        $recValue = trim($recValue, '[');
        $recValue = trim($recValue, '"');

        return $recValue;
    }
}
