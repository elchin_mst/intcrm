<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends Controller
{
    /**
     * @Route("/lol")
     */
    public function getAction()
    {
        $arr = array(
            'jsonrpc' => "2.0",
            'id' => "req1",
            'method' => "login.user",
            'params' => array(
                'login' => 'marketing@solarfields.ru',
                'password' => 'x5tsIE7y',
            ),
        );

        $infoApi = $this->httpReqest(json_encode($arr));

        $infoApi = json_decode($infoApi, true);

        $token = $infoApi['result']['data']['access_token'];

        $arr2 = array(
        'jsonrpc' => "2.0",
        'method' => "start.employee_call",
        'id' => "req1",
        'params' => array(
        'access_token' => $token,
        'first_call' => "employee",
        'switch_at_once' => true,
        'media_file_id' => 2701,
        'show_virtual_phone_number' => false,
        'virtual_phone_number' => "74993720692",
        'external_id' => "334otr01",
        'dtmf_string' => ".1.2.3",
        'direction' => "in",
        'contact' => "79260000000",
        'employee' => array(
            'id' => 25,
            'phone_number' => "79260000001"
        )
        )
    );

        return new Response(
            '<html><body> ' . $token . '</body></html>'
        );
    }

    public function httpReqest($kek)
    {
        $ch = curl_init("https://callapi.comagic.ru/v4.0");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type'=>'application/json'));
        //curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $kek);
        $output = curl_exec($ch);
        curl_close($ch);
        //echo $output;

        return $output;
    }

    public function callApi($topkek)
    {
        $ch = curl_init("https://callapi.comagic.ru/v4.0");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type'=>'application/json'));
        //curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $topkek);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }
}

